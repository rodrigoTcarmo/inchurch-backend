# API Information  #

API NAME: REGISTRON.

### Project structure ###

* registron package: Default project created by django
* registronapi package: Main project
* registronAUTH package: Separate app for password change functionality
* registrondeparts package: Separate app for manage departments
* static / templates folder: Basic front-end
* tests package: Package created just to run tests
* venv folder: Virtual environment

### How to run ###

* At the root of the project, we must activate the virtual environment executing this command line: "venv/Scripts/activate"
* Install all requirements listed in the "requirements.txt" file
* At the root of the project, run this command "python manage.py runserver"
* Access in the browser the link http://127.0.0.1:8000/

### What should I do? ###

* At first moment you'll see a login page, you can create a new user, or you can access using the admin profile:
####Username: registron_admin
####Password: Registron@00

* The user registron_admin has full permission within the API.
* New users have default "user" permission, they can only see users from the same department as theirs, delete his/her own profile, can't create or delete new departments.

### Do you have any questions? ###

Contact: rod.carmo@outlook.com | +55 21 99840-3045

### Thank you! : ) ###